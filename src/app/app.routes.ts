import { AboutComponent } from './components/about/about.component';
import { ContactComponent } from './components/contact/contact.component';
import { CvComponent } from './components/cv/cv.component';
import { HomeComponent } from './components/home/home.component';
import { PortfolioComponent } from './components/portfolio/portfolio.component';
import { BlogComponent } from './components/blog/blog.component';

export const APP_ROUTES	=	
[
	{ path: '', component: HomeComponent },
	{ path: 'about', component: AboutComponent },
	{ path: 'cv', component: CvComponent },
	{ path: 'portfolio', component: PortfolioComponent },
	{ path: 'blog', component: BlogComponent },
	{ path: 'contact', component: ContactComponent }
];
