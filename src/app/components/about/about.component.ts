import { Component, OnInit } from '@angular/core';
import { fadeInAnimation } from '../../app.router.animations';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css'],
  animations: [fadeInAnimation],
  host: { '[@fadeInAnimation]': 'true' }
})
export class AboutComponent implements OnInit 
{
	constructor() { }

	ngOnInit() {}
 }
