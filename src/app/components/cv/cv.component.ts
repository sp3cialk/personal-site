import { Component, OnInit } from '@angular/core';
import { fadeInAnimation } from '../../app.router.animations';

@Component({
  selector: 'app-cv',
  templateUrl: './cv.component.html',
  styleUrls: ['./cv.component.css'],
  animations: [fadeInAnimation],
  host: { '[@fadeInAnimation]': 'true' }
})
export class CvComponent implements OnInit 
{
   	constructor() { }

	ngOnInit() {}
}
