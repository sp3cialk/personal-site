import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { CvComponent } from './components/cv/cv.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { PortfolioComponent } from './components/portfolio/portfolio.component';
import { ContactComponent } from './components/contact/contact.component';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { BlogComponent } from './components/blog/blog.component';
import { APP_ROUTES } from './app.routes';
import { SafeReadmePipe } from './pipes/safe-readme-pipe';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    CvComponent,
    NavbarComponent,
    PortfolioComponent,
    ContactComponent,
    HomeComponent,
	AboutComponent,
	BlogComponent,
	SafeReadmePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
	HttpModule,
	RouterModule.forRoot(APP_ROUTES),
	BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {} 
