import { Component, OnInit } from '@angular/core';
import { fadeInAnimation } from '../../app.router.animations';
import { RepositoryService } from '../../services/repository/repository.service';
import { HttpConfig } from '../../config/http-conf';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css'],
  animations: [fadeInAnimation],
  providers: [RepositoryService],
  host: { '[@fadeInAnimation]': 'true' }
})
export class PortfolioComponent implements OnInit 
{
	projects;
	activeProject;

	constructor(private repositoryService: RepositoryService) 
	{
		this.loadRepositories();
	}

	ngOnInit() {}

	private loadRepositories()
	{
		this.repositoryService.getRepositories().subscribe(data => 
		{
			this.projects		=	data;
			this.setActiveProject(this.projects[0]);
		});
	}

	getActiveProjectReadme()
	{
		return this.getActiveProjectProperty('readme');
	}

	getActiveProjectUrl()
	{
		return this.getActiveProjectProperty('html_url');	
	}

	private getActiveProjectProperty(propertyName)
	{
		return this.activeProject != null ? this.activeProject[propertyName] : '';
	}

	setActiveProject(project)
	{
		this.activeProject			=	project;

		this.repositoryService.getReadme(this.activeProject).subscribe(data =>
		{
			this.activeProject.readme	=	data;
		});
	}

	private filterReadme()
	{
		
	}
}
