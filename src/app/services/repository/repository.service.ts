import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { HttpConfig } from '../../config/http-conf';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';

@Injectable()
export class RepositoryService 
{
	constructor(private http: Http) { }

	getRepositories()
	{
		var url		=	`${HttpConfig.apiUrl}/users/${HttpConfig.user}/repos`;
		
		return this.fetchJson(url);
	}

	getReadme(project)
	{
		var url				=	`${HttpConfig.apiUrl}/repos/${project.full_name}/readme`
		var headerParams	=	new Headers({ 'Accept': 'application/vnd.github.VERSION.html' });
		var options			=	new RequestOptions({ headers: headerParams });

		return this.http.get(url, options)
		.map((res:Response) => 
		{
			var readmeText		=	res.text();
			var filteredReadme	=	this.filterReadme(project, readmeText);

			return filteredReadme;
		});
	}

	private filterReadme(project, readme)
	{
		return this.filterReadmeImages(project, readme);

	}

	private filterReadmeImages(project, readme)
	{
		var imageBaseUrl	=	`${HttpConfig.userContentUrl}/${project.full_name}/master/`;
		return readme.replace(/src="preview/gi, 'src="' + imageBaseUrl + "/preview");
	}
	
	private fetchJson(url)
	{
		return this.http.get(url)
		.map((res:Response) => res.json());
	}
}

