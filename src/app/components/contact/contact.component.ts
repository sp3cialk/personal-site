import { Component, OnInit } from '@angular/core';
import { fadeInAnimation } from '../../app.router.animations';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],
  animations: [fadeInAnimation],
  host: { '[@fadeInAnimation]': 'true' }
})
export class ContactComponent implements OnInit 
{
   	constructor() { }

	ngOnInit() {}
}
