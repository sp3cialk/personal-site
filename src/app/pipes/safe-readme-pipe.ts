import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ReadmePipe } from './readme-pipe';

@Pipe({ name: 'safeReadmePipe' })
export class SafeReadmePipe implements PipeTransform
{
	constructor(private sanitized: DomSanitizer) {}

	transform(value)
	{
		if(value == null) return '';

		else
		{
			var readmePipe	=	new ReadmePipe();
			value			=	readmePipe.transform(value);

			return this.sanitized.bypassSecurityTrustHtml(value);
		}
	}
}
