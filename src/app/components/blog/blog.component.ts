import { Component, OnInit } from '@angular/core';
import { fadeInAnimation } from '../../app.router.animations';

@Component({
	selector: 'app-blog',
	templateUrl: './blog.component.html',
	styleUrls: ['./blog.component.css'],
	animations: [fadeInAnimation],
  	host: { '[@fadeInAnimation]': 'true' }
})
export class BlogComponent implements OnInit 
{
	constructor() {}

	ngOnInit() {}
}
